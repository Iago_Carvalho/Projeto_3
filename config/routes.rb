Rails.application.routes.draw do

  #get 'post/new'
  resources :posts
  resources :users
  resources :sessions
#  get 'welcome/homepage'
  get 'login' => 'sessions#new'
  
   root 'welcome#homepage'

end
